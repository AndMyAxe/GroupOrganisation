# Contact Information

This is a list of contact information for people who work with this group.

If you want to add your contact information here feel free. Remember that
anything put here is publicly viewable so make sure you are ok with this
information being public.

If you want to be part of this or contribute or just know who is here use the
inmysocks entry as a template for your own.

## inmysocks

With encouragement of a bunch of other people I started this. Mastodon is best for contacting me publicly, Tox is best for private messaging.

- Tox ID: 63FF9442651C2CEF6A788E45E7525835F98AC2AB78CE5B92413CDC144C805040606EFEE8E207
- Mastodon: @inmysocks@mastodon.social
- GitHub: inmysocks
- GitLab: @inmysocks

## Ekaitz

I like to take part in things. Mastodon is best for contacting me publicly, Tox is best for private messaging.

- Tox ID: F9FD4230BCB73095B62302182B0AF2AE7711D6F7AF571FF4FC960D81FD434011DBF53B8AE70F
- Mastodon: @ekaitz_zarraga@mastodon.social
- Github: ekaitz-zarraga
- Gitlab: ekaitz-zarraga
- Company website: http://elenq.tech
