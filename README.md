# What is this?

This is the general overview of the group.

You can use our <a href='StatementOfPurpose.md'>statement of purpose</a> if you
want a quick description.

## How do I get involved?

Either start contributing to an existing project or creating a project of your
own. If you don't know how or don't want to start a project on gitlab than let
another member know and we will make it for you or add wherever your project is
based to where we have projects.

Talk to us!

The general contact email is andmyaxe@fastmail.com

Currently all of our projects are on GitLab so joining the group on gitlab and
contributing to a project is probably the easiest way. If you want to start a
project than joining and starting a project under the AndMyAxe group works.

It is best if you talk to us first. Mastodon or the Matrix channel are the most
effective ways to do that.

### If you have a gitlab account you can join the group by

 - Go to the group page https://gitlab.com/AndMyAxe
 - Click on `Members` in the sidebar to the left
 - Click the Join Group button.

### Talk to us on Matrix

  - You can talk to us using Matrix in the channel #andmyaxe:matrix.org

### Finding member contact info

You can find contact information for group members by going to the
GroupOrganisation project wiki here:
https://gitlab.com/AndMyAxe/GroupOrganisation/wikis/home

On the sidebar to the left under the heading `members` there are pages for each
member with contact information.

## Basic Principles

* **Non-coercive**
  No one has to contribute anymore than they want. If you just help out for 20
  minutes once every two weeks that is fine. There is no requirement of
  expectation of anything other than you wanting to be here and being
  respectful (and following the code of conduct).
* **Collaborative**
  For a project to be part of And My Axe it has to be collaborative. This isn't
  a judgment about non-collaborative projects, just that this space is for
  collaborative work. That doesn't mean you have to give up ownership of your
  project, but if your project is part of And My Axe than you are giving
  permission for any member to work on the project. If two people have
  incompatible visions of what a project should be than we will create a new
  project if that is reasonable to do. If it isn't than the original creator of
  the project has final say on the direction.
* **Promote a healthy commons**
  This group is here to give us all a community where we can collaborate and
  help each other. In order to support this we need a healthy commons with good
  content.

One important aspect of all of this is documentation. Others can't use what you
create unless it is documented. This doesn't mean you have to do all the
documentation for a project yourself, but if you want to contribute to
something than documentation is a good place to start.

We need to explain what a project is for, what it does, how to use it and how
it works so that others can contribute to it.

**A note about ownership** - Projects on here have to have some sort of open
license. For software probably some flavor of BSD, MIT or GPL, for creative
works something version of Creative Commons or a similar license. I belive that
hardware designs would also get some type of Creative Commons license.
This is meant to help enrich the commons and it is also necessary to allow open
collaboration in this setting. The selection of the specific license to use is
up to the project creator. If you don't know what to use than just ask and
other members well help you decide.

## Licenses

The preferred licenses to use for projects if you don't have a reason to pick
another license are:

* Server software - AGPL
* Non-server software - MIT
* Creative works - CC-BY
* Hardware design - CC-BY

These are just suggestions, you don't have to use any of the licenses listed.
If you don't want your work to be used in a commercial context than no one will
be upset if you use CC-BY-NC or CC-BY-NC-ND or any other license. The only time
that may be a problem is if you pick a license that makes collaboration
difficult which would make the project a poor fit for this group.

# Things we need

- a group avatar
- copy editing for this
- other stuff?
